CHARTRES :PATRACDR 
Matos : Nécessaire uniquement – vous portez ce que vous prenez
MUSETTE : 
(Affaires de combat)
•	Protec perso : BAB, Genouillères, Coudières, Lunettes, Gants 
•	Chapeau de brousse (facultatif)
•	Bâche (facultatif)
•	Matériel de nettoyage
•	Gourde (remplie)
•	Lampe
•	Boussole
(Matos perso)
•	Carnet de combat (CDG-CDS)
•	Outillage perso
•	Nourriture perso
•	Peinture visage
•	Scotch/chatterton
•	Guêtres (facultatif)
•	Trousse 1r secours : soins courant (coupures, etc)
PAX :
(TNG (treillis) + HAIX) :
•	Attributs haute visu : sur le terrain 0 bandes patro
•	Carnet et stylo
•	Papier perso (carte d'identité militaire, civile, permis)
•	Béret
•	Facultatif : serflex, cyalume, jumelle
(Gilet de combat) : facultatif on aura des GPB
(Casque lourd) : perception
(Armement) : perception 
F2/TAP :
(Affaires de vie)
•	Vêtements de rechange (x10 jours)
•	Treillis de rechange
•	Nécessaire de toilette
•	Sac de couchage (duvet, drap (facultatif), oreiller (facultatif))
•	Tong
(Effets chauds/pluie)
•	Polaire
•	Parka
•	Foulard perso (camouflage CE ou vert)
(Affaires de sport) : ON NE RESTE PAS 10 JOURS SANS RIEN FAIRE
•	Tenue réglementaire ou tenue civile (SOBRE : noir/bleu)
•	Chaussures de sport
•	Maillot de bain
Complément de matériel perso : FACULATIF
•	Filet à linge (si machine à dispo)
•	Corde à linge
•	Lessive main ou dosettes si machine
•	2 cintres
•	Talc + straps
•	Hamac
•	PQ
•	Amélio : enceinte connectée
COMPLÉMENTS D’INFORMATION :
But de la manip :
•	Balisage des pistes
•	Garde des hélicos du 14 juillet (J/N)
•	Liaison des VIP 
Arrivée :
•	Jeudi 6/07 ou Vendredi 7/07 avant 8h
FINEX :
•	Samedi 15/07
