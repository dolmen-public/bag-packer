import { computed, reactive, ref, watch } from 'vue'

export class Item {
  id: string
  name: string

  constructor(name: string) {
    this.name = name
    this.id = Date.now().toString()
  }
}

export const items = reactive<Map<string, Item>>(new Map<string, Item>())

export const sortedItems = computed(() =>
  [...items.values()].sort((a, b) => a.name.localeCompare(b.name))
)

export const focusItemId = ref<Item['id']>('')

export const draggedItemId = ref<Item['id'] | null>(null)

const ITEMS_STORE_NAME = 'items'

watch(items, () => {
  localStorage.setItem(ITEMS_STORE_NAME, JSON.stringify([...items.values()]))
})

async function load() {
  const strItems = localStorage.getItem(ITEMS_STORE_NAME)
  if (strItems) {
    const itemToLaod = JSON.parse(strItems)
    for (const item of itemToLaod) {
      items.set(item.id, item)
    }
  }
}
load()
