import type { Item } from './Item'

export enum Slot {
  bag = 'bag',
  combatBag = 'combatBag',
  self = 'self',
}

export function slotParse(slot: string): Slot {
  if (slot === Slot.self) return Slot.self
  if (slot === Slot.combatBag) return Slot.combatBag
  return Slot.bag
}

export type JsonStorage = Array<[Item['id'], string]>

export class Storage extends Map<Item['id'], Slot> {
  toJSON(): JsonStorage {
    return [...this.entries()]
  }

  static fromJson(jsonStorage: JsonStorage): Storage {
    const storage = new Storage()
    if (jsonStorage.length) {
      for (const pair of jsonStorage) {
        storage.set(pair[0], slotParse(pair[1]))
      }
    }
    return storage
  }
}
