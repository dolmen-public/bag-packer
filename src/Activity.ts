import { reactive, ref, watch } from 'vue'
import { draggedItemId } from './Item'
import { Slot, Storage, type JsonStorage } from './Storage'

export class Activity {
  id: string
  name: string
  storage = new Storage()

  constructor(name: string) {
    this.name = name
    this.id = Date.now().toString()
  }

  static fromJson(jsonActivity: JsonActivity) {
    const activity = new Activity(jsonActivity.name)
    activity.id = jsonActivity.id
    activity.storage = Storage.fromJson(jsonActivity.storage)
    return activity
  }
}

type JsonActivity = {
  id: string
  name: string
  storage: JsonStorage
}

export const activities = reactive<Map<string, Activity>>(
  new Map<string, Activity>()
)

export function addDraggedItem(activityId: Activity['id'], slot: Slot) {
  if (draggedItemId.value) {
    activities.get(activityId)?.storage.set(draggedItemId.value, slot)
    draggedItemId.value = null
  }
}

export const focusActivity = ref<Activity['id']>('')

const ITEMS_STORE_NAME = 'activities'

watch(activities, () => {
  console.log('saving', activities)
  localStorage.setItem(
    ITEMS_STORE_NAME,
    JSON.stringify([...activities.values()])
  )
})

async function load() {
  const strs = localStorage.getItem(ITEMS_STORE_NAME)
  if (strs) {
    const objects = JSON.parse(strs)
    for (const object of objects) {
      try {
        const activity = Activity.fromJson(object)
        activities.set(activity.id, activity)
      } catch (e) {
        console.error(e)
      }
    }
  }
}
load()
